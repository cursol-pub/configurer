package env

import (
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/cursol-pub/configurer"
	"reflect"
)

func ReadConfig[T any]() (*T, error) {
	value := new(T)

	if err := envconfig.Process(configurer.EnvPrefix, value); err != nil {
		return nil, fmt.Errorf("envconfig process: %v", err)
	}

	if v, ok := reflect.TypeOf(value).(configurer.Validator); ok {
		if err := v.Validate(); err != nil {
			return nil, err
		}
	}

	return value, nil
}
