package env

import (
	"os"
	"testing"
)

type env struct {
	ConfigSource string `envconfig:"CONFIG_SOURCE"`
}

func TestReadConfig(t *testing.T) {
	if err := beforeTest(); err != nil {
		t.Fatal(err)
	}

	config, err := ReadConfig[env]()
	if err != nil {
		t.Error(err)
	}

	if config.ConfigSource != "env" {
		t.Errorf("wrong config value. wanted=env, got=%s", config.ConfigSource)
	}
}

func beforeTest() error {
	return os.Setenv("CONFIG_SOURCE", "env")
}
