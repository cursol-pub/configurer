package configurer

// Validator consumer has to provide own implementation of
// the validation interface
type Validator interface {
	// Validate performs config validation
	Validate() error
}
