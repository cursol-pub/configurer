package file

import (
	"encoding/json"
	"fmt"
	"gitlab.com/cursol-pub/configurer"
	"gopkg.in/yaml.v3"
	"os"
	"reflect"
	"strings"
)

func ReadConfig[T any](name string) (*T, error) {
	value := new(T)

	if name == "" {
		return nil, configurer.ErrEmptyConfigFilePath
	}

	cfgFile, err := os.ReadFile(name)
	if err != nil {
		return nil, fmt.Errorf("config read: %s", err)
	}

	switch {
	case strings.HasSuffix(name, configurer.ExtJSON):
		if err := json.Unmarshal(cfgFile, value); err != nil {
			return nil, fmt.Errorf("config unmarshal: %v", err)
		}
	case strings.HasSuffix(name, configurer.ExtYAML) || strings.HasSuffix(name, configurer.ExtYML):
		if err := yaml.Unmarshal(cfgFile, value); err != nil {
			return nil, fmt.Errorf("config unmarshal: %v", err)
		}
	default:
		return nil, fmt.Errorf("unsupported configuration file: %s", name)
	}

	if v, ok := reflect.TypeOf(value).(configurer.Validator); ok {
		if err = v.Validate(); err != nil {
			return nil, err
		}
	}

	return value, nil
}
