package awss3

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"gitlab.com/cursol-pub/configurer"
	"gopkg.in/yaml.v3"
	"reflect"
	"strings"
)

// ReadConfig Loads config by source and name from AWS S3 bucket
func ReadConfig[T any](ctx context.Context, bucket, key string) (*T, error) {
	value := new(T)

	conf, err := config.LoadDefaultConfig(ctx)
	if err != nil {
		return nil, err
	}

	svc := s3.NewFromConfig(conf)

	input := &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	}

	result, err := svc.GetObject(ctx, input)
	if err != nil {
		return nil, err
	}

	defer result.Body.Close()

	switch {
	case strings.HasSuffix(key, configurer.ExtJSON):
		if err := json.NewDecoder(result.Body).Decode(value); err != nil {
			return nil, fmt.Errorf("config unmarshal: %v", err)
		}
	case strings.HasSuffix(key, configurer.ExtYAML) || strings.HasSuffix(key, configurer.ExtYML):
		if err := yaml.NewDecoder(result.Body).Decode(value); err != nil {
			return nil, fmt.Errorf("config unmarshal: %v", err)
		}
	default:
		return nil, fmt.Errorf("unsupported configuration file: %s", key)
	}

	if v, ok := reflect.TypeOf(value).(configurer.Validator); ok {
		if err = v.Validate(); err != nil {
			return nil, err
		}
	}

	return value, nil
}
