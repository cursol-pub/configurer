package configurer

import (
	"github.com/pkg/errors"
)

const (
	EnvPrefix  = ""
	JsonPrefix = "{"
	JsonSuffix = "}"
	ExtJSON    = "json"
	ExtYAML    = "yaml"
	ExtYML     = "yml"
)

var (
	ErrEmptyConfigFilePath = errors.New("path to config file is empty")
)
