package awssm

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/secretsmanager"
	"gitlab.com/cursol-pub/configurer"
	"gopkg.in/yaml.v3"
	"reflect"
	"strings"
)

func ReadConfig[T any](ctx context.Context, secretId string) (*T, error) {
	value := new(T)

	conf, err := config.LoadDefaultConfig(ctx)
	if err != nil {
		return nil, err
	}

	svc := secretsmanager.NewFromConfig(conf)

	input := &secretsmanager.GetSecretValueInput{
		SecretId: aws.String(secretId),
	}

	result, err := svc.GetSecretValue(ctx, input)
	if err != nil {
		return nil, err
	}

	raw := *result.SecretString

	switch {
	case strings.HasPrefix(raw, configurer.JsonPrefix) && strings.HasSuffix(raw, configurer.JsonSuffix):
		if err := json.Unmarshal([]byte(raw), value); err != nil {
			return nil, fmt.Errorf("config unmarshal: %v", err)
		}
	default:
		if err := yaml.Unmarshal([]byte(raw), value); err != nil {
			return nil, fmt.Errorf("config unmarshal: %v", err)
		}
	}

	if v, ok := reflect.TypeOf(value).(configurer.Validator); ok {
		if err = v.Validate(); err != nil {
			return nil, err
		}
	}

	return value, nil
}
